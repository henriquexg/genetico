#include "locations.h"

Locations::Locations(string path, int amount_locations, bool GRID){
    this->amount_locations = amount_locations;

    if(!GRID){
        openConfig(path);
        doPositions();
    }
}

void Locations::openConfig(string path){
    cout<<"Opening config ..."<<endl;
    string fullPathConfig = path + "/config/config.txt";
    string::size_type sz;

    ifstream file;
    file.open(fullPathConfig);
    string output;
    int count = 0;
    if (file.is_open()) {
        while (!file.eof()) {
            file >> output;
            if(count == 0){
                this->AMOUNT_EXHIBITIONS = std::stoi (output,&sz);
                cout<<"Amount of Exhibitions: "<<this->AMOUNT_EXHIBITIONS<<endl;
            }
            if(count == 1){
                this->visitors = std::stoi (output,&sz);
                cout<<"Amount of Visitors: "<<this->visitors<<endl;
            }
            count++;
        }
        file.close();
    }

    if(this->amount_locations < this->AMOUNT_EXHIBITIONS) this->amount_locations = (int) (this->AMOUNT_EXHIBITIONS * 1.5);
}

void Locations::doPositions(){
    vector<Position>::iterator it;

    for(int i = 0; i < this->amount_locations; i++){
        int x = i;
        int y = i+1;

        this->positions.push_back(Position(x, y));
    }
}

void Locations::grid(int GRID_WIDTH, int GRID_HEIGHT){
    int i,j;

    this->positions.clear();

    this->AMOUNT_EXHIBITIONS = GRID_WIDTH*GRID_HEIGHT;

    for(i=1; i <= GRID_WIDTH; i++){
        for(j=1; j <= GRID_HEIGHT; j++){
            positions.push_back(Position(i, j));
        }
    }
}

void Locations::printPositions(){
    vector<Position>::iterator it;
    int count = 0;

    for(it = positions.begin(); it != positions.end(); it++, count++){
        cout<<"Position: "<<count<<endl;
        cout<<"X: "<<it->x<<"\t";
        cout<<"Y: "<<it->y<<endl;
    }
}

void Locations::destruction(){
    this->positions.clear();
}

#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

#include <iostream>
#include <vector>

#include "position.h"

using namespace std;

class Individual
{
public:
    Individual();
    void printGenes();
    void printEval();

    vector<Position> genes;
    double evaluation;
    unsigned int selectionProb;
};

#endif // INDIVIDUAL_H

#include "recommendation.h"

Recommendation::Recommendation(MatrixGraph *W_MATRIX){
    this->W_MATRIX = W_MATRIX;
    this->exp = EXPONENT;
    this->baseline = BASELINE;

    int i, j;

    int sum_matrix = 0;
    for(i = 0; i < this->W_MATRIX->AMOUNT_EXHIBITIONS; i++){
        for(j = 0; j < this->W_MATRIX->AMOUNT_EXHIBITIONS; j++){
            this->W_MATRIX->matrix.at((unsigned long) i).at((unsigned long) j)++;

            sum_matrix += this->W_MATRIX->matrix.at((unsigned long) i).at((unsigned long) j);
        }
    }
    this->W_MATRIX->SUM_MATRIX = sum_matrix;

    for(i = 0; i < this->W_MATRIX->AMOUNT_EXHIBITIONS; i++){
        int count = 0;
        for(j = 0; j < this->W_MATRIX->AMOUNT_EXHIBITIONS; j++){
            count += this->W_MATRIX->searchMatrix(j, i);
        }
        this->aPrioriExhibition.push_back(count);
    }
}

void Recommendation::printWeight(){
    vector<int>::iterator it;
    for(it = this->aPrioriExhibition.begin(); it != this->aPrioriExhibition.end(); it++){
        cout<<*it<<endl;
    }
}

int Recommendation::requestRecommendation(vector<Exhibition> &exhibitions){
    vector<Exhibition>::iterator itExhibitions;

    vector<double> weights;
    vector<double>::iterator itWeights;

    int index, i;

    double total = 0.0;

    if(this->baseline > 0){
        for(index = (int) (exhibitions.size() - this->baseline); index < exhibitions.size(); index++){
            double weight = pow(this->exp, (double)index);
            weights.push_back(weight);
            total = total + weight;
        }

        double ft = 1.0/total;
        total = 0;

        for(itWeights = weights.begin(); itWeights != weights.end(); itWeights++){
            *itWeights = *itWeights*ft;
            total = total + *itWeights;
        }

        itWeights--;
        *itWeights = *itWeights + (1.0-total);
    }

    this->evaluations.clear();
    double evaluation = 0.0;

    for(i = 0; i < this->W_MATRIX->AMOUNT_EXHIBITIONS; i++){
        evaluation = 0.0;
        for(itExhibitions = exhibitions.end()-this->baseline, itWeights = weights.begin(); itExhibitions != exhibitions.end(); itExhibitions++, itWeights++){
            evaluation += (*itWeights)*(log2(((double)W_MATRIX->searchMatrix((unsigned long) itExhibitions->idExhibition, (unsigned long) i))/((double)this->aPrioriExhibition.at((unsigned long) i))));
        }
        evaluation += log2((double)this->aPrioriExhibition.at((unsigned long) i)/(double)this->W_MATRIX->SUM_MATRIX);

        evaluations.push_back(pow(2.0,evaluation));
    }

    return bestClassification(exhibitions);
}

void Recommendation::printRecom(){
    vector<double>::iterator it;
    int i;

    for(it = this->evaluations.begin(), i = 0; it != this->evaluations.end(); it++, i++){
        cout<<i<<" - "<<*it<<endl;
    }
}

int Recommendation::bestClassification(vector<Exhibition> &exhibitions){
    vector<double>::iterator it;
    int i, index = 0;

    double best = 0.0;

    for(it = this->evaluations.begin(), i = 0; it != this->evaluations.end(); it++, i++){
        if(*it >= best && !exists(exhibitions, i)){
            index = i;
            best = *it;
        }
        else if(*it >= best && exhibitions.size() >= this->W_MATRIX->AMOUNT_EXHIBITIONS){
            index = i;
            best = *it;
        }
    }

    return index;
}

bool Recommendation::exists(vector<Exhibition> &exhibitions, int i){
    vector<Exhibition>::iterator it;

    for(it = exhibitions.begin(); it != exhibitions.end(); it++){
        if(it->idExhibition == i){
            return true;
        }
    }

    return false;
}

double Recommendation::evaluate(vector<Exhibition> &recommended, vector<Exhibition> &original, int &cut){
    if(recommended.size() != original.size()){
        return -1.0;
    }

    double a = 0.0, b = 0.0, numerator = 0.0, denominator = 0.0;

    for(unsigned long i = recommended.size() - ((unsigned long) cut); i < recommended.size(); i++){
        a = (double) (original.at(i).idExhibition+1);
        b = (double) (recommended.at(i).idExhibition+1);

        numerator += (double) a*b;
        denominator += (double) min(a,b)*min(a,b);
    }

    return 1.0-(sqrt(pow((1.0-(numerator/denominator)), 2.0)));
}

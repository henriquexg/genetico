#include "visitor.h"

Visitor::Visitor()
{

}

bool Visitor::exists(int idExhibition){
    vector<Exhibition>::iterator it;

    for(it = this->exhibitions.begin(); it != this->exhibitions.end(); it++){
        if(it->idExhibition == idExhibition){
            return true;
        }
    }
    return false;
}

void Visitor::print(){
    vector<Exhibition>::iterator it;

    for(it = this->exhibitions.begin(); it != this->exhibitions.end(); it++){
        cout<<"| Exib: "<<it->idExhibition<<" ";
    }cout<<endl;
}

void Visitor::weight(){
    vector<Exhibition>::iterator it;

    vector<double> weights;
    vector<double>::iterator itW;
    int i = 1;
    double total = 0.0;

    for(; i <= this->exhibitions.size(); i++){
        weights.push_back(pow(2.0, (double)-i));
        total = total + pow(2.0, (double)-i) ;
    }

    double ft = ((double)RAND_MAX)/total;
    total = 0;

    for(itW = weights.begin(); itW != weights.end(); itW++){
        *itW = *itW*ft;
        total = total + *itW;
    }

    itW--;
    *itW = *itW + (((double)RAND_MAX)-total);


    for(it = this->exhibitions.begin(), itW = weights.begin(); it != this->exhibitions.end(); it++, itW++){
        it->weight = *itW;
    }
}

void Visitor::insertExhibition(Exhibition &exib){
    this->exhibitions.push_back(exib);
}

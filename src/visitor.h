#ifndef VISITOR_H
#define VISITOR_H

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include "exhibition.h"

using namespace std;

class Visitor
{
public:
    Visitor();
    bool exists(int idExhibition);
    void print();
    void weight();

    void insertExhibition(Exhibition &exib);

    string idVisitor;
    vector<Exhibition> exhibitions;
};

#endif // VISITOR_H

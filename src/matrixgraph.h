#ifndef MATRIXGRAPH_H
#define MATRIXGRAPH_H

#include <iostream>
#include <cstdlib>
#include <QFile>
#include <QDir>
#include <fstream>

using namespace std;

class MatrixGraph
{
public:
    MatrixGraph(int amountExib, string path);
    void clean();
    void addMatrix(unsigned long origin, unsigned long destination);
    void addValueMatrix(unsigned long origin, unsigned long destination, int value);
    void printMatrix();
    void data();
    int searchMatrix(unsigned long origin, unsigned long destination);

    vector<vector<int>> matrix;

    int AMOUNT_EXHIBITIONS;
    int SUM_MATRIX;

private:
    string path;
};

#endif // MATRIXGRAPH_H

#include "individual.h"

Individual::Individual()
{
    this->evaluation = 0;
    this->selectionProb = 0;
}

void Individual::printGenes(){
    vector<Position>::iterator it;

    for(it = this->genes.begin(); it != this->genes.end(); it++){
        cout<<it->x<<"-"<<it->y<<"\t";
    }
    cout<<endl;
}

void Individual::printEval(){
    cout<<"Evaluation: "<<this->evaluation<<endl;
}

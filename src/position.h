#ifndef POSITION_H
#define POSITION_H

class Position
{
public:
    Position(int x, int y);

    int x;
    int y;
};

inline bool operator ==(const Position& a, const Position& b){
    return a.x == b.x && a.y == b.y;
}

inline bool operator !=(const Position& a, const Position& b){
    return !(a.x == b.x && a.y == b.y);
}

#endif // POSITION_H

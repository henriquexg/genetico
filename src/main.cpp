#include <iostream>
#include <QString>
#include <QDir>
#include <fstream>

#include "locations.h"
#include "matrixgraph.h"
#include "geneticalgorithm.h"
#include "roulette.h"
#include "visitor.h"
#include "recommendation.h"

using namespace std;

int GRID_WIDTH;
int GRID_HEIGHT;
MatrixGraph* W_MATRIX;
Roulette ROULETTE;
Locations* locationsNotGRID;

vector<Visitor> visitors;
vector<string> exhibitions;

bool visitorExists(string& idVisitor){
	vector<Visitor>::iterator it;

	for(it = visitors.begin(); it != visitors.end(); it++){
		if(it->idVisitor == idVisitor){
			return true;
		}
	}
	return false;
}

void dataSetNotGrid(string path){
	cout<<"Opening file..."<<endl;

	string open_visits_file = path + "config/visits.txt";
	string open_exhibitions_file = path + "config/exhibitions.txt";

	fstream fsVisit, fsExhibition;

	fsVisit.open(open_visits_file.c_str());
	fsExhibition.open(open_exhibitions_file.c_str());

	if(!fsVisit.is_open() || !fsExhibition.is_open()) {
		cout<<"ERROR - DATA SET !"<<endl;
		system("exit");
		return;
	}

	string lineExhibition;

	getline(fsExhibition, lineExhibition);
	while(!fsExhibition.eof()){
		getline(fsExhibition, lineExhibition);
		exhibitions.push_back(lineExhibition);
	}

	string line;
	getline(fsVisit, line);
	while(!fsVisit.eof()){
		getline(fsVisit, line);
		int pos = (int) line.find("@");
		string visitorID = line.substr((unsigned long) (pos + 1), line.length() - pos);
		string exhibitionID = line.substr(0, (unsigned long) pos);

		int exist = visitorExists(visitorID);

		if(exist == 0){
			Visitor visitor;
			Exhibition exhibition(QString(exhibitionID.c_str()).toInt(),  0);
			visitor.idVisitor = visitorID;
            visitor.insertExhibition(exhibition);
			visitors.push_back(visitor);
		}

		if(exist == 1){
			Exhibition exhibition(QString(exhibitionID.c_str()).toInt(), 0);

			vector<Visitor>::iterator it;

			for(it = visitors.begin(); it != visitors.end(); it++){
				if(it->idVisitor == visitorID){
                    it->insertExhibition(exhibition);
				}
			}
		}

		if(QString(exhibitionID.c_str()).toInt()%1 == 0) cout<<exhibitionID<<" | "<<visitorID<<"\r";
	}
	fsExhibition.close();
	fsVisit.close();

	cout<<"Exhibitions - amount: "<<exhibitions.size()<<endl;
	cout<<"Visitors - amount: "<<visitors.size()<<endl;
}

void cleanData(string pathData){
	cout<<"Cleaning directory..."<<endl;

	QString pathDir = QString(pathData.c_str());
	QDir dir(pathDir);

	if(!dir.exists()){
		QDir().mkdir(pathData.c_str());
	}

	dir.setNameFilters(QStringList() << "*.*");
	dir.setFilter(QDir::Files);
	foreach(QString dirFile, dir.entryList())
	{
		dir.remove(dirFile);
	}
}

void rouletteFunc(string path){
	string pathData = path + "data/";

    cleanData(pathData);

	system("echo `date`");
	dataSetNotGrid(path);

	vector<Visitor>::iterator it;

	for(it = visitors.begin(); it != visitors.end(); it++){
        it->weight();
	}

	system("echo `date`");
	cout<<"ROULETTE SORTING..."<<endl;

	for(it = visitors.begin(); it != visitors.end(); it++){
		ROULETTE.reset();

		ROULETTE.exhibitions = it->exhibitions;

        ROULETTE.run();

		it->exhibitions = ROULETTE.visits;

		vector<Exhibition>::iterator itFS;
		ofstream fs;

		string file = pathData + it->idVisitor + "_data.txt";

		fs.open(file.c_str());

		 for(itFS = it->exhibitions.begin(); itFS != it->exhibitions.end(); itFS++){
			 fs<<to_string(itFS->idExhibition) + "\n";
		 }
		fs.close();
	}

	cout<<"Visits - amount: "<<ROULETTE.amount_edges<<endl;

	system("echo `date`");
}

void openDataSetNotGrid(string pathData){
	cout<<"Opening data ..."<<endl;
	QString pathDir = QString(pathData.c_str());
	QDir dir(pathDir);

	if(!dir.exists()){
		QDir().mkdir(pathData.c_str());
	}

	dir.setNameFilters(QStringList() << "*.txt");
	dir.setFilter(QDir::Files);

    foreach(QString dirFile, dir.entryList()){
		ifstream file;
		file.open(pathData+dirFile.toStdString());
		string output;
		string::size_type sz;
		int origin, destination;

        if (file.is_open()){
			file >> output;
			origin = std::stoi (output,&sz);

			while (!file.eof()) {

				file >> output;
				destination = std::stoi (output,&sz);

				if(origin==destination){
					continue;
				}

                W_MATRIX->addMatrix((unsigned long) origin, (unsigned long) destination);
				origin = destination;
			}
			file.close();
        }
	}
}

void createDataSetGrid(MatrixGraph &matrix){
    unsigned long i, j;
    int count_h, count_w;

	for(i = 1, j = 2, count_h = 1; count_h <= GRID_HEIGHT; count_h++){
	   for(count_w = 1; count_w <= GRID_WIDTH; count_w++, i++, j++){
		   if(count_w != GRID_WIDTH){
               matrix.addValueMatrix(j - 1, i - 1, (rand() % 100) + 1);
               matrix.addValueMatrix(i - 1, j - 1, (rand() % 100) + 1);
		   }
		   if(count_h != GRID_HEIGHT){
               matrix.addValueMatrix(i - 1, i + GRID_WIDTH - 1, (rand() % 100) + 1);
               matrix.addValueMatrix(i + GRID_WIDTH - 1, i - 1, (rand() % 100) + 1);
		   }
	   }
	}
}

MatrixGraph* getMatrixGrid(string path){
	string fileMatrix = path + "config/matrixGrid.txt";
	string fileMatrixConfig = path + "config/configMatrixGrid.txt";

	ifstream file, fileConfig;
	file.open(fileMatrix);
	fileConfig.open(fileMatrixConfig);
	string output;
	string::size_type sz;

	fileConfig >> output;
	GRID_WIDTH = stoi (output,&sz);
	fileConfig >> output;
	GRID_HEIGHT = stoi (output,&sz);

	fileConfig.close();

	MatrixGraph* W = new MatrixGraph(GRID_HEIGHT*GRID_WIDTH, path);

    unsigned long i,j;

	for (i = 0; i < GRID_WIDTH*GRID_HEIGHT; i++) {
		for (j = 0; j < GRID_WIDTH*GRID_HEIGHT; j++) {
			file >> output;
            W->addValueMatrix(i, j, stoi(output, &sz));
		}
	}

	cout<<"\nVisits - amount: "<<W->SUM_MATRIX<<endl<<endl;

	return W;
}

void GA(int argc, char *argv[], string path, bool GRID){

	bool RANDOM_ALG;
	bool mutation_test;
	bool exhibitions_test;
	bool mutation_crossover_test;
    bool crossover_test;

    if(argc != 23){
		RANDOM_ALG = true;
		mutation_test = false;
		exhibitions_test = false;
        crossover_test = false;
	}
	else{
		RANDOM_ALG = (bool)atoi(argv[7]);
		mutation_test = (bool)atoi(argv[4]);
		exhibitions_test = (bool)atoi(argv[14]);
		mutation_crossover_test = (bool)atoi(argv[15]);
        crossover_test = (bool)atoi(argv[21]);
	}

	if(atoi(argv[1]) > 50){
		cout<<"Invalid mutation value !"<<endl;
		return;
	}

	ofstream fs;

	if(GRID){
		GRID_WIDTH = atoi(argv[11]);
		GRID_HEIGHT = atoi(argv[12]);

		MatrixGraph* matrixGRID;

		if(!exhibitions_test){
			matrixGRID = getMatrixGrid(path);
		}
		else{
			matrixGRID = new MatrixGraph(GRID_WIDTH*GRID_HEIGHT, path);
			createDataSetGrid(*matrixGRID);
		}

		Locations locations(path, GRID_WIDTH*GRID_HEIGHT, GRID);
		locations.grid(GRID_WIDTH, GRID_HEIGHT);

        GeneticAlgorithm geneticAlg(locations.positions, locations.AMOUNT_EXHIBITIONS*2, matrixGRID, path);

        if(argc != 23){
            geneticAlg.mutation_chance = 30;
			geneticAlg.max_generation_number = 10000;
			geneticAlg.M_MIN = 0.2;
			geneticAlg.crossover_chance = 100;
			geneticAlg.stop_generation = false;
			geneticAlg.stop_M = true;
			geneticAlg.stop_evaluation = true;
			geneticAlg.GRID = GRID;
			geneticAlg.max_evaluation = 1.0;
            geneticAlg.exhibitions_test = false;
		}
		else{
            geneticAlg.mutation_chance = atoi(argv[1]);
			geneticAlg.max_generation_number = atoi(argv[5]);
			QString argv2(argv[2]);
			geneticAlg.M_MIN = argv2.toDouble();
			QString argv3(argv[3]);
			geneticAlg.crossover_chance = argv3.toInt();
			geneticAlg.stop_generation = (bool)atoi(argv[8]);
			geneticAlg.stop_M = (bool)atoi(argv[9]);
			geneticAlg.stop_evaluation = (bool)atoi(argv[10]);
			geneticAlg.GRID = GRID;
			QString argv13(argv[13]);
			geneticAlg.max_evaluation = argv13.toDouble();
            geneticAlg.exhibitions_test = exhibitions_test;
		}

		if(RANDOM_ALG) geneticAlg.randomAlgorithmInit();
		geneticAlg.loop(RANDOM_ALG);

		locations.destruction();
        geneticAlg.clear();

		if(mutation_test){
            stringstream ssMutation;
            ssMutation << geneticAlg.mutation_chance;
            string mut = ssMutation.str();

            stringstream ssCrossover;
            ssCrossover << geneticAlg.crossover_chance;
            string cross = ssCrossover.str();

            string pathLog;

            if(mutation_crossover_test){
                pathLog = path + "mutations/crossMut_" + mut + "_" + cross + ".txt";
            }
            else{
                pathLog = path + "mutations/mutation_"+ mut +".txt";
            }

            fs.open(pathLog.c_str(), ios_base::app);
            fs<<geneticAlg.best_evaluation<<"\n";
            fs.flush();
            fs.close();
		}

        if(crossover_test){
            stringstream ssCrossover;
            ssCrossover << geneticAlg.crossover_chance;
            string cross = ssCrossover.str();

            string pathLog;

            pathLog = path + "logs/log_crossover.txt";

            fs.open(pathLog.c_str(), ios_base::app);
            fs<<geneticAlg.crossover_chance<<"@"<<geneticAlg.best_evaluation<<"\n";
            fs.flush();
            fs.close();
        }

        delete matrixGRID;
	}
	else{
		GeneticAlgorithm geneticAlg(locationsNotGRID->positions, locationsNotGRID->AMOUNT_EXHIBITIONS*2, W_MATRIX, path);

        if(argc != 23){
            geneticAlg.mutation_chance = 30;
			geneticAlg.max_generation_number = 10000;
			geneticAlg.M_MIN = 0.2;
			geneticAlg.crossover_chance = 100;
			geneticAlg.stop_generation = false;
			geneticAlg.stop_M = true;
			geneticAlg.stop_evaluation = true;
			geneticAlg.GRID = GRID;
			geneticAlg.max_evaluation = 1.0;
            geneticAlg.exhibitions_test = false;
		}
		else{
            geneticAlg.mutation_chance = atoi(argv[1]);
			geneticAlg.max_generation_number = atoi(argv[5]);
			QString argv2(argv[2]);
			geneticAlg.M_MIN = argv2.toDouble();
			QString argv3(argv[3]);
			geneticAlg.crossover_chance = argv3.toInt();
			geneticAlg.stop_generation = (bool)atoi(argv[8]);
			geneticAlg.stop_M = (bool)atoi(argv[9]);
			geneticAlg.stop_evaluation = (bool)atoi(argv[10]);
			geneticAlg.GRID = GRID;
			QString argv13(argv[13]);
			geneticAlg.max_evaluation = argv13.toDouble();
            geneticAlg.exhibitions_test = exhibitions_test;
		}

		if(RANDOM_ALG) geneticAlg.randomAlgorithmInit();
		geneticAlg.loop(RANDOM_ALG);

		if(mutation_test){
            stringstream ssMutation;
            ssMutation << geneticAlg.mutation_chance;
            string mut = ssMutation.str();

            stringstream ssCrossover;
            ssCrossover << geneticAlg.crossover_chance;
            string cross = ssCrossover.str();

            string pathLog;

            if(mutation_crossover_test){
                pathLog = path + "mutations/crossMut_" + mut + "_" + cross + ".txt";
            }
            else{
                pathLog = path + "mutations/mutation_"+ mut +".txt";
            }

            fs.open(pathLog.c_str(), ios_base::app);
            fs<<geneticAlg.best_evaluation<<"\n";
            fs.flush();
            fs.close();
		}

        if(crossover_test){
            stringstream ssCrossover;
            ssCrossover << geneticAlg.crossover_chance;
            string cross = ssCrossover.str();

            string pathLog;

            pathLog = path + "logs/log_crossover.txt";

            fs.open(pathLog.c_str(), ios_base::app);
            fs<<geneticAlg.crossover_chance<<"@"<<geneticAlg.best_evaluation<<"\n";
            fs.flush();
            fs.close();
        }
	}

}

void doRecommendation(string path, string pathData, char *argv[]){// 10875_data.txt has only 9 visits in actual dataset
    Recommendation recommendation(W_MATRIX);
	double evaluation = 0.0;
	int cut = atoi(argv[18]);
	recommendation.baseline = atoi(argv[19]);

	Visitor visitor;
	QString pathDir = QString(pathData.c_str());
	QDir dir(pathDir);

    dir.setNameFilters(QStringList() << "*_data.txt");
	dir.setFilter(QDir::Files);

	foreach(QString dirFile, dir.entryList()){
		ifstream file;
		file.open(pathData+dirFile.toStdString());
		string output;

		if (file.is_open()) {
			while (getline (file,output)) {
				Exhibition e(QString(output.c_str()).toInt(), 0.0);
                visitor.insertExhibition(e);
			}
			file.close();
		}
        Visitor visitor_original = visitor;

		visitor.exhibitions.erase(visitor.exhibitions.end()-cut, visitor.exhibitions.end());

		for(int i = 1; i <= cut; i++){
            int add_recommendation = recommendation.requestRecommendation(visitor.exhibitions);
            //cout<<i<<" - Next Exhibition: "<<add_recommendation<<endl;
			visitor.exhibitions.push_back(Exhibition(add_recommendation,0.0));
		}

        evaluation += recommendation.evaluate(visitor.exhibitions, visitor_original.exhibitions, cut);

		visitor.exhibitions.clear();
	}
	cout<<cut<<" - "<<evaluation/(double)dir.entryList().size()<<endl;

	ofstream fs;
	stringstream ssRatio, ssCut;
	ssRatio<<fixed<<setprecision(17)<<evaluation/(double)dir.entryList().size();
	string ratio = ssRatio.str();

	ssCut<<cut;
    string CUT_STRING = ssCut.str();

	string pathLog = path + "logs/recommendation.txt";
	fs.open(pathLog.c_str(), ios_base::app);
    fs<<CUT_STRING + "@" + ratio<<"\n";
	fs.flush();
	fs.close();
}

int main(int argc, char *argv[]){
	srand((unsigned)time(0));
	cout.precision(17);

	string home = QDir::homePath().toStdString();
	string path = home + "/genetico/";
	string pathData = path + "data/";

    bool ROULETTE = (bool)atoi(argv[20]);
	if(ROULETTE){
        rouletteFunc(path);
	}

    bool AMOUNT_LOCATIONS = (bool)atoi(argv[22]);

	bool GRID = (bool)atoi(argv[6]);
	if(!GRID){

        locationsNotGRID = new Locations(path, AMOUNT_LOCATIONS, GRID);
        W_MATRIX = new MatrixGraph(locationsNotGRID->AMOUNT_EXHIBITIONS, path);

        openDataSetNotGrid(pathData);

		if((bool)atoi(argv[17])){
            doRecommendation(path, pathData, argv);
		}
	}

	if((bool)atoi(argv[16])){
		GA(argc, argv, path, GRID);
	}

	delete W_MATRIX;
	delete locationsNotGRID;

	return 0;
}

#include "matrixgraph.h"

MatrixGraph::MatrixGraph(int amountExib, string path){
    this->path = path;
    this->AMOUNT_EXHIBITIONS = amountExib;
    this->SUM_MATRIX = 0;

    vector<int> vec;
    vec.resize((unsigned long) this->AMOUNT_EXHIBITIONS, 0);

    int i;
    for (i = 0; i < this->AMOUNT_EXHIBITIONS; i++) {
        this->matrix.push_back(vec);
    }
}
void MatrixGraph::clean(){
    for (unsigned long i = 0; i < AMOUNT_EXHIBITIONS; i++){
      this->matrix.at(i).clear();
    }
    this->matrix.clear();
}

void MatrixGraph::addMatrix(unsigned long origin, unsigned long destination){
    if(origin >= this->AMOUNT_EXHIBITIONS || destination >= this->AMOUNT_EXHIBITIONS || origin < 0 || destination < 0){
        cout<<"Invalid position in matrix !"<<endl;
        return;
    }

    this->matrix.at(origin).at(destination)++;
    this->SUM_MATRIX++;
}

void MatrixGraph::addValueMatrix(unsigned long origin, unsigned long destination, int value){
    if(origin >= this->AMOUNT_EXHIBITIONS || destination >= this->AMOUNT_EXHIBITIONS || origin < 0 || destination < 0){
        cout<<"Invalid position in matrix !"<<endl;
        return;
    }

    this->matrix.at(origin).at(destination) = value;
    this->SUM_MATRIX += value;
}

void MatrixGraph::printMatrix(){
    cout<<endl;
    unsigned long i,j;
    for (i = 0; i < AMOUNT_EXHIBITIONS; i++) {
        for (j = 0; j < AMOUNT_EXHIBITIONS; j++) {
            cout<<this->matrix.at(i).at(j)<<" ";
        }
        cout<<endl;
    }

    cout<<"\nAmount Visits: "<<this->SUM_MATRIX<<endl<<endl;
}

void MatrixGraph::data(){
    ofstream fs;
    string file = path + "config/matrix.txt";

    QFile ArqEx(file.c_str());
    if(ArqEx.exists())
    {
        QFile::remove(file.c_str());
    }

    fs.open(file.c_str(), ios_base::app);

    unsigned long i,j;

    for (i = 0; i < AMOUNT_EXHIBITIONS; i++) {
        for (j = 0; j < AMOUNT_EXHIBITIONS; j++) {
            fs<<this->matrix.at(i).at(j)<<"\n";
            fs.flush();
        }
    }

    fs.close();
}

int MatrixGraph::searchMatrix(unsigned long origin, unsigned long destination){
    if(origin >= this->AMOUNT_EXHIBITIONS || destination >= this->AMOUNT_EXHIBITIONS || origin < 0 || destination < 0){
        cout<<"Invalid position in matrix !"<<endl;
        return -1;
    }

    return this->matrix.at(origin).at(destination);
}

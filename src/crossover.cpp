#include "crossover.h"

Crossover::Crossover(Individual a, Individual b, int cut1, int cut2)
{
    this->a = a;
    this->b = b;
    this->cut1 = cut1;
    this->cut2 = cut2;
}

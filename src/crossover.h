#ifndef CROSSOVER_H
#define CROSSOVER_H

#include "individual.h"

class Crossover
{
public:
    Crossover(Individual a, Individual b, int cut1, int cut2);

    Individual a;
    Individual b;
    int cut1;
    int cut2;
};

#endif // CROSSOVER_H

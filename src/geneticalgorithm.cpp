﻿#include "geneticalgorithm.h"

GeneticAlgorithm::GeneticAlgorithm(vector<Position> &positions, int amount_individuals, MatrixGraph* W_MATRIX, string path){
    this->generation_number = 0;
    this->count_stop_condition = 0;

    this->W_MATRIX = W_MATRIX;
    this->positions = positions;
    this->amount_individuals = amount_individuals;

    if(positions.size() < W_MATRIX->AMOUNT_EXHIBITIONS){
        cout<<"Number of positions or exhibition is invalid !"<<endl;
        return;
    }

    for(int i = 0; i < this->amount_individuals && this->amount_individuals >= 2; i++){
        initialIndividual();
    }
}

void GeneticAlgorithm::initialIndividual(){
    vector<Position> positionsCopy = this->positions;
    Individual individual;
    vector<Position>::iterator it;
    int i;

    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    shuffle(positionsCopy.begin()+1,positionsCopy.end(), default_random_engine(seed));

    for(i = 0, it=positionsCopy.begin(); i < this->W_MATRIX->AMOUNT_EXHIBITIONS; i++, it++){
        individual.genes.push_back(*it);
    }

    evaluate(individual);

    this->population.push_back(individual);
}

void GeneticAlgorithm::clear(){
    vector<Individual>::iterator it;

    for(it = this->population.begin(); it != this->population.end(); it++){
        it->genes.clear();
    }

    for(it = this->future_population.begin(); it != this->future_population.end(); it++){
        it->genes.clear();
    }

    this->random_individual.genes.clear();
    this->population.clear();
    this->future_population.clear();
    this->positions.clear();
    this->crossover_queue.clear();
}

bool GeneticAlgorithm::stopCondition(double evaluation){
    if(this->count_stop_condition == 0){
        this->x1 = this->generation_number;
        this->y1 = evaluation;
    }

    if(this->count_stop_condition == 50){
        this->x2 = this->generation_number;
        this->y2 = evaluation;

        this->M = (y2-y1)/(x2-x1);

        if((this->stop_generation && this->generation_number >= this->max_generation_number) || (this->stop_M && this->M <= this->M_MIN) || (this->stop_evaluation && this->GRID && (this->best_evaluation/this->W_MATRIX->SUM_MATRIX) >= this->max_evaluation)){
            if(this->exhibitions_test){
                ofstream fs;
                string file = this->path + "logs/exhibitions.txt";

                fs.open(file.c_str(), ios_base::app);
                if(this->GRID){
                    fs<<this->W_MATRIX->AMOUNT_EXHIBITIONS<<"@"<<evaluation/this->W_MATRIX->SUM_MATRIX<<"\n";
                }
                else{
                    fs<<this->W_MATRIX->AMOUNT_EXHIBITIONS<<"@"<<evaluation<<"\n";
                }

                fs.flush();
                fs.close();
            }

            return false;
        }

        this->count_stop_condition = 0;
    }

    this->count_stop_condition++;
    return true;
}

void GeneticAlgorithm::loop(bool RANDOM_ALG){
    int ajust_crossover = 1;
    bool unpaired = false;

    string file_path_genetic = this->path + "logs/log_genetic.txt";
    string file_path_random = this->path + "logs/log_random.txt";

    QFile file_genetic(file_path_genetic.c_str());
    if(file_genetic.exists())
    {
        QFile::remove(file_path_genetic.c_str());
    }
    QFile file_random(file_path_random.c_str());
    if(file_random.exists())
    {
        QFile::remove(file_path_random.c_str());
    }

    if(amount_individuals%2 != 0){
        unpaired = true;
        ajust_crossover = 2;
    }

    if(this->mutation_chance > 50){
        cout<<"Mutation value is invalid !"<<endl;
        return;
    }

    do{

        elitism(unpaired);

        selection(ajust_crossover);

        logGeneration(RANDOM_ALG, file_path_genetic);

        nextGeneration();

        this->generation_number++;

    }while(stopCondition(this->best_evaluation));
}

void GeneticAlgorithm::elitism(bool &unpaired){
    double best1 = 0.0, best2 = 0.0, best3 = 0.0;

    vector<Individual>::iterator it, itM1, itM2, itM3;

    for(it = this->population.begin(); it != this->population.end(); it++){
        if(it->evaluation >= best1){
            best1 = it->evaluation;
            itM1 = it;
            this->best_individual = *it;
        }
    }
    this->future_population.push_back(*itM1);

    for(it = this->population.begin(); it != this->population.end(); it++){
        if(it->evaluation >= best2 && it->evaluation != best1){
            best2 = it->evaluation;
            itM2 = it;
        }
    }
    this->future_population.push_back(*itM2);

    if(unpaired){
        for(it = this->population.begin(); it != this->population.end(); it++){
            if(it->evaluation > best3 && it->evaluation != best1 && it->evaluation != best2){
                best3 = it->evaluation;
                itM3 = it;
            }
        }
        this->future_population.push_back(*itM3);
    }
}

void GeneticAlgorithm::selection(int &ajust_crossover){
    vector<Individual>::iterator it;

    Individual a;
    Individual b;

    int cut1, cut2;

    double sum_evaluates = 0.0;

    for(it=this->population.begin(); it != this->population.end(); it++){
        sum_evaluates = sum_evaluates + it->evaluation;
    }

    unsigned int sum = 0;

    for(it=this->population.begin(); it != this->population.end(); it++){
        it->selectionProb = (unsigned int) round((it->evaluation/sum_evaluates)*RAND_MAX);
        sum += it->selectionProb;
    }

    it--;
    int alpha = RAND_MAX - sum;
    it->selectionProb += alpha;


    int random_number, amount_crossover = 0;

    while(amount_crossover != (amount_individuals-ajust_crossover)/2){
        for(int amount_individuals_crossover = 0; amount_individuals_crossover < 2; amount_individuals_crossover++){

            random_number = rand() % RAND_MAX;

            double sort_count = 0.0;

            for(it=this->population.begin(); it != this->population.end(); it++){
                sort_count = sort_count + it->selectionProb;
                if(sort_count >= random_number){
                    if(amount_individuals_crossover == 0){
                        a = *it;
                        break;
                    }
                    else if(amount_individuals_crossover == 1){
                        b = *it;
                        if(equal(a.genes.begin(), a.genes.end(), b.genes.begin(), b.genes.end())){
                            amount_individuals_crossover--;
                            break;
                        }
                        cut1 = (rand() % (this->W_MATRIX->AMOUNT_EXHIBITIONS - 2));
                        cut2 = (rand() % (this->W_MATRIX->AMOUNT_EXHIBITIONS - 1));
                        while(cut2 <= cut1){
                            cut2 = (rand() % (this->W_MATRIX->AMOUNT_EXHIBITIONS - 1));
                        }

                        Crossover c(a, b, cut1+1, cut2+1);
                        this->crossover_queue.push_back(c);

                        amount_crossover++;

                        break;
                    }
                }
            }
        }
    }

    QtConcurrent::blockingMap(this->crossover_queue, [this](Crossover &c){
       crossover(c.a, c.b, c.cut1, c.cut2);
    });

    vector<Crossover>::iterator itCross;

    for(itCross = this->crossover_queue.begin(); itCross != this->crossover_queue.end(); itCross++){
        this->future_population.push_back(itCross->a);
        this->future_population.push_back(itCross->b);
    }
}

void GeneticAlgorithm::logGeneration(bool& RANDOM_ALG, string& fileGA){
    string result = this->path + "logs/result.txt";

    ofstream file_generation, file_position;

    vector<Position>::iterator itPosition;

    this->best_evaluation = this->best_individual.evaluation;

    if(this->generation_number%LOG_PRINT == 0 && GRID){
        cout<<"Evaluation: "<<this->best_evaluation<<" | Generation: "<<this->generation_number<<" | Precision: "<<this->best_evaluation/this->W_MATRIX->SUM_MATRIX<<" | Amount of Individuals: "<<future_population.size()<<" | Amount of Exhibitions: "<<this->W_MATRIX->AMOUNT_EXHIBITIONS<<" | M: "<<this->M<<" | Mutation: "<<this->mutation_chance<<"%"<< " | Crossover: "<<this->crossover_chance<<"%"<<endl;
    }
    else if(this->generation_number%LOG_PRINT == 0){
        cout<<"Evaluation: "<<this->best_evaluation<<" | Generation: "<<this->generation_number<<" | Amount of Individuals: "<<future_population.size()<<" | Amount of Exhibitions: "<<this->W_MATRIX->AMOUNT_EXHIBITIONS<<" | M: "<<this->M<<" | Mutation: "<<this->mutation_chance<<"%"<< " | Crossover: "<<this->crossover_chance<<"%"<<endl;
    }

    if(RANDOM_ALG) randomAlgorithm();

    file_generation.open(fileGA.c_str(), ios_base::app);

    if(this->GRID){
        file_generation<<this->generation_number<<"@"<<this->best_evaluation/this->W_MATRIX->SUM_MATRIX<<"\n";
        file_generation.flush();
    }
    else{
        file_generation<<this->generation_number<<"@"<<this->best_evaluation<<"\n";
        file_generation.flush();
    }

    file_generation.close();

    file_position.open(result.c_str());
    for(itPosition = this->best_individual.genes.begin(); itPosition != this->best_individual.genes.end(); itPosition++){
        file_position<<itPosition->x<<"@"<<itPosition->y<<"\n";
        file_position.flush();
    }
    file_position.close();
}

void GeneticAlgorithm::crossover(Individual &a, Individual &b, int &cut1, int &cut2){
    Individual child_A;
    Individual child_B;

    int crossSort = rand() % 100;

    if(crossSort < this->crossover_chance && !equal(a.genes.begin(), a.genes.end(), b.genes.begin(), b.genes.end())){
        vector<int> A_index, B_index, child_A_index, child_B_index;
        A_index = doIndex(a.genes);
        B_index = doIndex(b.genes);

        vector<int>::iterator itIndex1, itIndex2;

        for(itIndex1 = A_index.begin(), itIndex2 = B_index.begin(); (itIndex1 != A_index.end()) && (itIndex2 != B_index.end()); itIndex1++, itIndex2++){
            if(child_A_index.size() == cut1 || child_B_index.size() == cut1){
                break;
            }
            child_A_index.push_back(*itIndex1);
            child_B_index.push_back(*itIndex2);
        }

        for(itIndex1 = A_index.begin() + cut1, itIndex2 = B_index.begin() + cut1; (itIndex1 != A_index.end()) && (itIndex2 != B_index.end()); itIndex1++, itIndex2++){
            if(child_A_index.size() == cut2 || child_B_index.size() == cut2){
                break;
            }
            child_A_index.push_back(*itIndex2);
            child_B_index.push_back(*itIndex1);
        }

        for(itIndex1 = A_index.begin() + cut2, itIndex2 = B_index.begin() + cut2; (itIndex1 != A_index.end()) && (itIndex2 != B_index.end()); itIndex1++, itIndex2++){
            child_A_index.push_back(*itIndex1);
            child_B_index.push_back(*itIndex2);
        }

        child_A.genes = getIndex(child_A_index);
        child_B.genes = getIndex(child_B_index);

    }
    else if(crossSort < this->crossover_chance){
        child_A = a;
        child_B = b;

        mutation(child_A, 100);
        mutation(child_B, 100);
    }
    else{
        child_A = a;
        child_B = b;
    }

    if(child_A.genes.size() == this->W_MATRIX->AMOUNT_EXHIBITIONS){
        mutation(child_A);
        mutation(child_B);

        evaluate(child_A);
        evaluate(child_B);

        a = child_A;
        b = child_B;
    }
    else{
        cout<<"Amount of genes is invalid !"<<endl;
    }
}

vector<int> GeneticAlgorithm::doIndex(vector<Position> &vec){
    vector<int> index;
    vector<Position> positionsCopy = this->positions;

    vector<Position>::iterator it, it2;
    int i;

    for(it2 = vec.begin(); it2 != vec.end(); it2++){
        for(it = positionsCopy.begin(), i = 0; it != positionsCopy.end(); it++, i++){
            if(*it == *it2){
                index.push_back(i);
                break;
            }
        }
        positionsCopy.erase(positionsCopy.begin()+i);
    }

    return index;
}

vector<Position> GeneticAlgorithm::getIndex(vector<int>& vec){
    vector<Position> child;
    vector<Position> positionsCopy = this->positions;

    vector<Position>::iterator it;
    vector<int>::iterator it2;
    int i;

    for(it2 = vec.begin(); it2 != vec.end(); it2++){
        for(it = positionsCopy.begin(), i = 0; it != positionsCopy.end(); it++, i++){
            if(i == *it2){
                child.push_back(*it);
                break;
            }
        }
        positionsCopy.erase(positionsCopy.begin()+i);
    }

    return child;
}

void GeneticAlgorithm::mutation(Individual &a){
    int sort_number = (rand() % 100) + 1;

    if(sort_number <= this->mutation_chance){
        vector<Position>::iterator it2,it3;
        unsigned long sort_position = rand() % (a.genes.size() - 1);
        bool exists = false;

        vector<Position> away_positions;

        for(it2 = positions.begin(); it2 != positions.end(); it2++){
            for(it3 = a.genes.begin(); it3 != a.genes.end(); it3++){
                if(*it2 == *it3){
                    exists = true;
                    break;
                }
            }
            if(!exists){
                exists = false;
                away_positions.push_back(*it2);
            }
            else{
                exists = false;
            }
        }

        if(away_positions.size() == 0){
            return;
        }

        unsigned long location = rand() % away_positions.size();

        swap(away_positions.at(location), a.genes.at(sort_position+1));
    }

    if(sort_number > this->mutation_chance && sort_number <= this->mutation_chance*2){
        unsigned long exhibition1 = rand() % (a.genes.size() - 1);
        unsigned long exhibition2 = rand() % (a.genes.size() - 1);

        while(exhibition2 == exhibition1){
            exhibition2 = rand() % (a.genes.size() - 1);
        }

        swap(a.genes.at(exhibition1+1), a.genes.at(exhibition2+1));
    }
}

void GeneticAlgorithm::mutation(Individual &a, int mutationValue){
    int sort_number = (rand() % 100) + 1;

    if(mutationValue > 100 || mutationValue < 0){
        cout<<"Mutation value is invalid ! = "<<mutationValue<<endl;
        return;
    }

    if(sort_number <= mutationValue){
        unsigned long exhibition1 = rand() % (a.genes.size() - 1);
        unsigned long exhibition2 = rand() % (a.genes.size() - 1);

        while(exhibition2 == exhibition1){
            exhibition2 = rand() % (a.genes.size() - 1);
        }

        swap(a.genes.at(exhibition1+1), a.genes.at(exhibition2+1));

        vector<Position>::iterator it2,it3;
        unsigned long sort_position = rand() % (a.genes.size() - 1);
        bool exists = false;

        vector<Position> away_positions;

        for(it2 = positions.begin(); it2 != positions.end(); it2++){
            for(it3 = a.genes.begin(); it3 != a.genes.end(); it3++){
                if(*it2 == *it3){
                    exists = true;
                    break;
                }
            }
            if(!exists){
                exists = false;
                away_positions.push_back(*it2);
            }
            else{
                exists = false;
            }
        }

        if(away_positions.size() == 0){
            return;
        }

        unsigned long location = rand() % away_positions.size();

        swap(away_positions.at(location), a.genes.at(sort_position+1));
    }
}

void GeneticAlgorithm::nextGeneration(){
    this->crossover_queue.clear();

    this->population = this->future_population;

    this->future_population.clear();

    return;
}

void GeneticAlgorithm::randomAlgorithmInit(){
    unsigned long sort = rand() % this->population.size();

    this->random_individual = this->population.at(sort);

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    shuffle(this->random_individual.genes.begin(), this->random_individual.genes.end(), std::default_random_engine(seed));
}

void GeneticAlgorithm::randomAlgorithm(){
    Individual random_individualCopy = this->random_individual;

    unsigned long sort1 = rand() % random_individual.genes.size();
    unsigned long sort2 = rand() % random_individual.genes.size();

    while(sort1 == sort2){
        sort2 = rand() % random_individual.genes.size();
    }

    swap(this->random_individual.genes.at(sort1), this->random_individual.genes.at(sort2));

    vector<Position>::iterator it2,it3;
    unsigned long sort_position = rand() % (random_individual.genes.size() - 1);
    bool exists = false;

    vector<Position> away_positions;

    for(it2 = positions.begin(); it2 != positions.end(); it2++){
        for(it3 = random_individual.genes.begin(); it3 != random_individual.genes.end(); it3++){
            if(*it2 == *it3){
                exists = true;
                break;
            }
        }
        if(!exists){
            exists = false;
            away_positions.push_back(*it2);
        }
        else{
            exists = false;
        }
    }

    if(away_positions.size() != 0){
        unsigned long location = rand() % away_positions.size();

        swap(away_positions.at(location), random_individual.genes.at(sort_position+1));
    }

    evaluate(random_individual);

    if(random_individual.evaluation < random_individualCopy.evaluation){
        random_individual = random_individualCopy;
    }

    ofstream fs;
    string file_random = path + "logs/log_random.txt";

    fs.open(file_random.c_str(), ios_base::app);
    stringstream ss;
    if(this->GRID){
        ss<<fixed<<setprecision(12)<<random_individual.evaluation/this->W_MATRIX->SUM_MATRIX;
    }
    else{
        ss<<fixed<<setprecision(12)<<random_individual.evaluation;
    }

    fs<<to_string(this->generation_number)+"@"+ss.str()+"\n";
    fs.flush();

    fs.close();
}

void GeneticAlgorithm::evaluate(Individual &a){
    vector<Position>::iterator it, it2;

    unsigned long x,y;
    double partial = 0.0;

    for(it = a.genes.begin(), x = 0; it != a.genes.end(); it++, x++){
        for(it2 = a.genes.begin(), y = 0; it2 != a.genes.end(); it2++, y++){
            if(x == y){
                continue;
            }

            double W = (double) this->W_MATRIX->matrix.at(x).at(y);
            if(W == -1){
                continue;
            }

            double __x = (double) it->x - it2->x;
            double __y = (double) it->y - it2->y;

            double d = sqrt((__x*__x)+(__y*__y));

            partial = partial + (W/d);
        }
    }
    a.evaluation = partial;
}

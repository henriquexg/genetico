#ifndef LOCATIONS_H
#define LOCATIONS_H

#include <iostream>
#include <cstdlib>
#include <QString>
#include <fstream>
#include <string>
#include <vector>

#include "position.h"

using namespace std;

class Locations
{
public:
    Locations(string path, int amount_locations, bool GRID);
    void openConfig(string path);
    void doPositions();
    void printPositions();
    void grid(int GRID_WIDTH, int GRID_HEIGHT);

    void destruction();

    vector<Position> positions;
    int AMOUNT_EXHIBITIONS;

private:
    int amount_locations;
    int visitors;
};

#endif // LOCATIONS_H

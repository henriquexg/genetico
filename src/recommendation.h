#ifndef RECOMMENDATION_H
#define RECOMMENDATION_H

#include "matrixgraph.h"
#include "visitor.h"
#include "exhibition.h"

#define BASELINE 3
#define EXPONENT 2.0;

class Recommendation
{
public:
    Recommendation(MatrixGraph* W_MATRIX);
    void printWeight();
    int requestRecommendation(vector<Exhibition> &exhibitions);
    void printRecom();
    double evaluate(vector<Exhibition> &recommended, vector<Exhibition> &original, int &cut);

    double exp;
    int baseline;

private:
    int bestClassification(vector<Exhibition> &exhibitions);
    bool exists(vector<Exhibition> &exhibitions, int i);

    MatrixGraph* W_MATRIX;
    vector<int> aPrioriExhibition;
    vector<double> evaluations;
};

#endif // RECOMMENDATION_H

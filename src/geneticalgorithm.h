#ifndef GENETICALGORITHM_H
#define GENETICALGORITHM_H

#include <iostream>
#include <cstdlib>
#include <QString>
#include <QFile>
#include <string>
#include <vector>
#include <cmath>
#include <thread>
#include <mutex>
#include <fstream>
#include <sstream>
#include <QDir>
#include <iomanip>
#include <random>
#include <algorithm>
#include <QtConcurrent>

#include "position.h"
#include "individual.h"
#include "matrixgraph.h"
#include "crossover.h"

#define LOG_PRINT 1000

using namespace std;

class GeneticAlgorithm
{
public:
    GeneticAlgorithm(vector<Position> &positions, int amount_individuals, MatrixGraph* W_MATRIX, string path);
    void randomAlgorithmInit();
    void loop(bool RANDOM_ALG);
    void clear();

    int generation_number;
    Individual best_individual;

    double best_evaluation;
    int crossover_chance;
    int mutation_chance;
    double max_evaluation;
    int max_generation_number;
    bool stop_generation;
    bool stop_M;
    bool stop_evaluation;
    bool GRID;
    double M_MIN;
    bool exhibitions_test;

private:
    void initialIndividual();
    void crossover(Individual &a, Individual &b, int &cut1, int &cut2);
    void mutation(Individual &a);
    void mutation(Individual &a, int mutationValue);
    void nextGeneration();
    bool stopCondition(double evaluation);
    void randomAlgorithm();
    void evaluate(Individual &a);
    vector<int> doIndex(vector<Position>& vec);
    vector<Position> getIndex(vector<int>& vec);
    void elitism(bool &unpaired);
    void selection(int &ajust_crossover);
    void logGeneration(bool &RANDOM_ALG, string &fileGA);

    vector<Crossover> crossover_queue;
    vector<Individual> population;
    vector<Individual> future_population;
    vector<Position> positions;

    double M;
    int amount_individuals;
    int count_stop_condition;
    int x1,x2;
    double y1,y2;
    string path;

    Individual random_individual;
    MatrixGraph* W_MATRIX;
};

#endif // GENETICALGORITHM_H

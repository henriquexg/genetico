#ifndef EXHIBITION_H
#define EXHIBITION_H

class Exhibition
{
public:
    Exhibition(int idExhibition, double weight);
    int idExhibition;
    double weight;
};

#endif // EXHIBITION_H

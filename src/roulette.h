#ifndef ROULETTE_H
#define ROULETTE_H

#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <cmath>

#include "exhibition.h"

using namespace std;

class Roulette
{
public:
    Roulette();
    void insertExhibition(int idExhibition, double weight);
    void sort();
    void run();
    double sumNormalize();
    void normalize();
    void printVisits();
    void order();
    void reset();

    vector<Exhibition> exhibitions;
    vector<Exhibition> visits;

    int amount_edges;
};

#endif // ROULETTE_H

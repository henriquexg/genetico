#include "roulette.h"

Roulette::Roulette()
{
    amount_edges = 0;
}

void Roulette::insertExhibition(int idExhibition, double weight){
    Exhibition exib(idExhibition,weight);

    this->exhibitions.insert(this->exhibitions.end(), exib);
}

void Roulette::order(){
    vector<Exhibition>::iterator itExib;
    vector<Exhibition>::iterator itAux;

    vector<Exhibition> aux;

    for(itExib = this->exhibitions.begin(); itExib != this->exhibitions.end(); itExib++){
        for(itAux = aux.begin(); itAux != aux.end() && itExib->weight >= itAux->weight; itAux++);
        aux.insert(itAux, *itExib);
    }

    this->exhibitions.clear();
    this->exhibitions = aux;
    aux.clear();
}

void Roulette::run(){
    this->order();
    int size = (int) this->exhibitions.size();

    for(int i = 0 ; i < size ; i++){
        this->sort();
    }
}

void Roulette::reset(){
    this->visits.clear();
    this->exhibitions.clear();
}

void Roulette::sort(){
    int num = rand()%RAND_MAX;

    vector<Exhibition>::iterator it;

    double pos = 0.0;
    for(it = this->exhibitions.begin(); it != this->exhibitions.end(); it++){
        pos = pos + it->weight;
        if(num <= pos){
            break;
        }
    }

    amount_edges++;

    visits.push_back(*it);
    this->exhibitions.erase(it);

    if(this->exhibitions.size() > 0) normalize();
}

double Roulette::sumNormalize(){
    vector<Exhibition>::iterator it;
    double sumNorm = 0;

    for(it = this->exhibitions.begin(); it != this->exhibitions.end(); it++){
        sumNorm = sumNorm + it->weight;
    }
    return sumNorm;
}

void Roulette::normalize(){
    vector<Exhibition>::iterator it;

    double sumNorm = sumNormalize();
    double prob = 0.0;

    for(it = this->exhibitions.begin(); it != this->exhibitions.end(); it++){
        it->weight = (it->weight/sumNorm)*((double)RAND_MAX);
        prob = prob + it->weight;
    }

    it--;
    it->weight = it->weight + (((double)RAND_MAX)-prob);
}

void Roulette::printVisits(){
    vector<Exhibition>::iterator it;

    for(it = visits.begin(); it != visits.end(); it++){
        cout<<it->idExhibition<<"\t"<<endl;
    }
}

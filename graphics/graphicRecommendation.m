data = load('~/genetico/logs/recommendation.txt');

size = 25;

x = reshape(data(:,1),size,1);
y = reshape(data(:,2),size,1);
z = reshape(data(:,3),size,1);

limitsX = linspace(min(x), max(x));
limitsY = linspace(min(y), max(y));

[Xout,Yout] = meshgrid(limitsX, limitsY);

Zout = griddata(x, y, z, Xout, Yout);

surf(Xout, Yout, Zout);

xlabel('Cut');
ylabel('Baseline');
zlabel('Evaluation');

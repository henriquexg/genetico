# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser

if __name__ == '__main__':
	home = expanduser("~")
	path = home + "/genetico/logs/log_genetic.txt"
	pathRandom = home + "/genetico/logs/log_random.txt"

	array = []
	generations = []
	evaluations = []

	if(os.path.exists(path)):
		with open(path, "r") as archive:
			for line in archive:
				for word in line.split():
					array.append(word)

		for line in array:
			separator = line.find('@')

			generation = line[:separator]
			generations.append(generation)

			evaluation = line[separator + 1:]
			evaluations.append(evaluation)

	arrayRandom = []
	generationsRandom = []
	evaluationsRandom = []

	if(os.path.exists(pathRandom)):
		with open(pathRandom, "r") as archive:
			for line in archive:
				for word in line.split():
					arrayRandom.append(word)

		for line in arrayRandom:
			separator = line.find('@')

			generation = line[:separator]
			generationsRandom.append(generation)

			evaluation = line[separator + 1:]
			evaluationsRandom.append(evaluation)

	plt.xlabel("Generation".decode("utf-8"))
	plt.ylabel("Evaluation".decode("utf-8"))
	plt.plot(generations, evaluations, 'b', generationsRandom, evaluationsRandom, 'r')
	plt.show()

# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser
import sys

if __name__ == '__main__':

	home = expanduser("~")

	mutation = 0
	crossover = 0

	MAXmut = 0
	MAXcross = 0
	MAXevaluation = 0

	p = []

	while(mutation <= 50):
		while(crossover <= 100):
			path = home + "/genetico/mutations/crossMut_" + str(mutation) + "_" + str(crossover) + ".txt"
			if(os.path.exists(path)):
				with open(path, "r") as archive:
					lines = 0
					count = 0

					for line in archive:
						lines += float(line)
						count += 1

					evaluationAVG = lines/count
					sys.stdout.write("Evaluation: " + str(evaluationAVG) + " Crossover: " + str(crossover) + " Mutation: " + str(mutation) + '\n')

					if(evaluationAVG > MAXevaluation):
						MAXevaluation = evaluationAVG
						MAXmut = mutation
						MAXcross = crossover

					point = []
					point.append(mutation)
					point.append(crossover)
					point.append(evaluationAVG)
					p.append(point)
			crossover += 10
		crossover = 0
		mutation += 10

	logCrossMut = "/genetico/logs/logCrossMut.txt"
	if(os.path.exists(home + logCrossMut)):
		os.system("rm " + home + logCrossMut)
	with open(home + logCrossMut, "w") as archive:
		for line in p:
			l = ""
			for i in line:
				l = l + str(i) + " "
			l = l + "\n"
			archive.write(l)

	sys.stdout.write("BEST -- Evaluation: " + str(MAXevaluation) + " Crossover: " + str(MAXcross) + " Mutation: " + str(MAXmut) + '\n')

	os.system("octave --no-gui --persist $HOME/genetico/graphics/graphicCrossMut.m")

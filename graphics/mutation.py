# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser

if __name__ == '__main__':

	home = expanduser("~")

	mutation = 0
	INCREMENT_MUTATION = 5

	mutations = []
	normalizations = []

	while(mutation != 55):
		path = home + "/genetico/mutations/mutation_" + str(mutation) + ".txt"
		if(os.path.exists(path)):
			evaluations = []
			sumEvaluations = 0
			normalizedEvaluation = 0
			with open(path, "r") as archive:
				for line in archive:
					for word in line.split():
						evaluations.append(word)
						sumEvaluations = sumEvaluations + float(word)

			normalizedEvaluation = sumEvaluations/len(evaluations)

			mutations.append(mutation)
			normalizations.append(normalizedEvaluation)

		mutation = mutation + INCREMENT_MUTATION

	mutation = 0

	mutationsMax = []
	normalizationsMax = []

	while(mutation != 55):
		path = home + "/genetico/mutations/mutation_" + str(mutation) + ".txt"
		if(os.path.exists(path)):
			normalizedEvaluationMAX = 0
			with open(path, "r") as archive:
				for line in archive:
					for word in line.split():
						if(float(word) > normalizedEvaluationMAX):
							normalizedEvaluationMAX = float(word)

			mutationsMax.append(mutation)
			normalizationsMax.append(normalizedEvaluationMAX)

		mutation = mutation + INCREMENT_MUTATION

	mutation = 0

	mutationsMin = []
	normalizationsMin = []

	while(mutation != 55):
		path = home + "/genetico/mutations/mutation_" + str(mutation) + ".txt"
		if(os.path.exists(path)):
			normalizedEvaluationMIN = normalizedEvaluationMAX
			with open(path, "r") as archive:
				for line in archive:
					for word in line.split():
						if(float(word) < normalizedEvaluationMIN):
							normalizedEvaluationMIN = float(word)

			mutationsMin.append(mutation)
			normalizationsMin.append(normalizedEvaluationMIN)

		mutation = mutation + INCREMENT_MUTATION

	plt.xlabel("Mutation (%)".decode("utf-8"))
	plt.ylabel("Evaluation".decode("utf-8"))
	plt.plot(mutations, normalizations, 'b', mutationsMax, normalizationsMax, 'r', mutationsMin, normalizationsMin, 'g')
	plt.show()

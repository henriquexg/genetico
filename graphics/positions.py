# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

if __name__ == '__main__':
	home = expanduser("~")
	path = home + "/genetico/logs/result.txt"

	positions = []

	with open(path, "r") as archive:
		for line in archive:
			for word in line.split():
				positions.append(word)

	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')

	xs = []
	ys = []
	zs = []

	for line in positions:
		separator = line.find('@')

		x = int(line[:separator])
		xs.append(x)

		y =  int(line[separator+1:])
		ys.append(y)

		z = int(1)
		zs.append(z)

		ax.scatter(xs, ys, zs, c='r')

	ax.set_xlabel('X')
	ax.set_ylabel('Y')
	ax.set_zlabel('Z')

	plt.show()

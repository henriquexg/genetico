data = load('~/genetico/logs/logCrossMut.txt');
x = reshape(data(:,1),66,1);
y = reshape(data(:,2),66,1);
z = reshape(data(:,3),66,1);

limitsX = linspace(min(x), max(x));
limitsY = linspace(min(y), max(y));

[Xout,Yout] = meshgrid(limitsX, limitsY);

Zout = griddata(x, y, z, Xout, Yout);

surf(Xout, Yout, Zout);

xlabel('Mutation (%)');
ylabel('Crossover (%)');
zlabel('Evaluation');

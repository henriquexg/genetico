# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser

if __name__ == '__main__':
    home = expanduser("~")
    path = home + "/genetico/logs/exhibitions.txt"

    array = []
    generations = []
    evaluations = []

    with open(path, "r") as archive:
        for line in archive:
            for word in line.split():
                array.append(word)

    for line in array:
        separator = line.find('@')

        generation = line[:separator]
        generations.append(generation)

        evaluation = line[separator + 1:]
        evaluations.append(evaluation)

    plt.xlabel("Amount of exhibitions".decode("utf-8"))
    plt.ylabel("Evaluation normalized between 0 and 1".decode("utf-8"))
    plt.plot(generations, evaluations)
    plt.show()

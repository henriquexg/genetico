# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import os
from os.path import expanduser

if __name__ == '__main__':
    home = expanduser("~")
    path = home + "/genetico/logs/log_crossover.txt"

    array = []
    cross = []
    evaluations = []

    evaluations100 = 0
    evaluations100n = 0
    evaluations90 = 0
    evaluations90n = 0
    evaluations80 = 0
    evaluations80n = 0
    evaluations70 = 0
    evaluations70n = 0
    evaluations60 = 0
    evaluations60n = 0
    evaluations50 = 0
    evaluations50n = 0
    evaluations40 = 0
    evaluations40n = 0
    evaluations30 = 0
    evaluations30n = 0
    evaluations20 = 0
    evaluations20n = 0
    evaluations10 = 0
    evaluations10n = 0
    evaluations0 = 0
    evaluations0n = 0

    if(os.path.exists(path)):
        with open(path, "r") as archive:
            for line in archive:
                for word in line.split():
                    array.append(word)

        for line in array:
            separator = line.find('@')

            crossoverValue = int(line[:separator])

            evaluation = float(line[separator + 1:])

            if(crossoverValue == 100):
                evaluations100 = evaluations100 + evaluation
                evaluations100n = evaluations100n + 1
            if(crossoverValue == 90):
                evaluations90 = evaluations90 + evaluation
                evaluations90n = evaluations90n + 1
            if(crossoverValue == 80):
                evaluations80 = evaluations80 + evaluation
                evaluations80n = evaluations80n + 1
            if(crossoverValue == 70):
                evaluations70 = evaluations70 + evaluation
                evaluations70n = evaluations70n + 1
            if(crossoverValue == 60):
                evaluations60 = evaluations60 + evaluation
                evaluations60n = evaluations60n + 1
            if(crossoverValue == 50):
                evaluations50 = evaluations50 + evaluation
                evaluations50n = evaluations50n + 1
            if(crossoverValue == 40):
                evaluations40 = evaluations40 + evaluation
                evaluations40n = evaluations40n + 1
            if(crossoverValue == 30):
                evaluations30 = evaluations30 + evaluation
                evaluations30n = evaluations30n + 1
            if(crossoverValue == 20):
                evaluations20 = evaluations20 + evaluation
                evaluations20n = evaluations20n + 1
            if(crossoverValue == 10):
                evaluations10 = evaluations10 + evaluation
                evaluations10n = evaluations10n + 1
            if(crossoverValue == 0):
                evaluations0 = evaluations0 + evaluation
                evaluations0n = evaluations0n + 1

    if(evaluations10n != 0):
        evaluations.append(evaluations0/evaluations0n)
        evaluations.append(evaluations10/evaluations10n)
        evaluations.append(evaluations20/evaluations20n)
        evaluations.append(evaluations30/evaluations30n)
        evaluations.append(evaluations40/evaluations40n)
        evaluations.append(evaluations50/evaluations50n)
        evaluations.append(evaluations60/evaluations60n)
        evaluations.append(evaluations70/evaluations70n)
        evaluations.append(evaluations80/evaluations80n)
        evaluations.append(evaluations90/evaluations90n)
        evaluations.append(evaluations100/evaluations100n)

    for i in range(0,11):
        cross.append(i*10)


    plt.xlabel("Crossover (%)".decode("utf-8"))
    plt.ylabel("Evaluation normalized between 0 and 1".decode("utf-8"))
    plt.plot(cross, evaluations, 'b')
    plt.show()

# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt;
import os;
from os.path import expanduser;
import sys;

if __name__ == '__main__':

	home = expanduser("~");

	baseline = 0;

	p = [];
	ratio = [];

	while(baseline <= 4):
		path = home + "/genetico/logs/recommendation" + str(baseline) + ".txt";
		if(os.path.exists(path)):
			with open(path, "r") as archive:
				actual = 0.0;
				count = 0;
				for line in archive:
					separador = line.find('@');
					cut = int(line[:separador]);
					evaluation = float(line[separador + 1:]);

					actual += evaluation;
					count += 1;

					sys.stdout.write("Evaluation: " + str(evaluation) + " Cut: " + str(cut) + " Baseline: " + str(baseline) + '\n');

					point = [];
					point.append(cut);
					point.append(baseline);
					point.append(evaluation);
					p.append(point);

				ratio.append(actual/float(count));
				sys.stdout.write("Evaluation ratio: " + str(actual/float(count)) + " Cut: " + str(cut) + " Baseline: " + str(baseline) + '\n');

		baseline += 1;

	logRecom = "/genetico/logs/recommendation.txt";
	if(os.path.exists(home + logRecom)):
		os.system("rm " + home + logRecom);
	with open(home + logRecom, "w") as archive:
		for line in p:
			l = "";
			for i in line:
				l = l + str(i) + " ";
			l = l + "\n";
			archive.write(l);

	os.system("octave --no-gui --persist $HOME/genetico/graphics/graphicRecommendation.m")

QT += core
QT += concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14

TARGET = genetico
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += src/main.cpp \
    src/locations.cpp \
    src/position.cpp \
    src/matrixgraph.cpp \
    src/individual.cpp \
    src/geneticalgorithm.cpp \
    src/crossover.cpp \
    src/roulette.cpp \
    src/visitor.cpp \
    src/exhibition.cpp \
    src/recommendation.cpp

HEADERS += \
    src/locations.h \
    src/position.h \
    src/matrixgraph.h \
    src/individual.h \
    src/geneticalgorithm.h \
    src/crossover.h \
    src/roulette.h \
    src/visitor.h \
    src/exhibition.h \
    src/recommendation.h

#!/bin/bash

rm -rf $HOME/genetico/logs/exibitions.txt

GRID_WIDTH=2
GRID_HEIGHT=2

SWITCH=0

while(((GRID_WIDTH*GRID_HEIGHT)<=100))
do
    $HOME/genetico/genetico 5 0.2 100 0 10000 1 0 0 1 1 $GRID_WIDTH $GRID_HEIGHT 1 1 1 0 3 3 0 0 150

    echo "GRID_WIDTH: $GRID_WIDTH | GRID_HEIGHT: $GRID_HEIGHT"

    if((SWITCH==0))
    then
        ((GRID_WIDTH++))
        ((SWITCH=1))
    else
        ((GRID_HEIGHT++))
        ((SWITCH=0))
    fi
done

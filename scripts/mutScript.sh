#!/bin/bash

rm -rf $HOME/genetico/mutations/mutation_*

MUTATION=0

MAX_MUTATION=50
MAX_EXECUTION_COUNT=10
MAX_GENERATION=50000
INCREMENT_MUTATION=5

while((MUTATION!=MAX_MUTATION+INCREMENT_MUTATION))
do
    $HOME/genetico/genetico $MUTATION 0.1 0 1 $MAX_GENERATION 0 0 1 0 1 10 10 1 0 0 1 0 3 3 0 0 150
    lcount=$(wc -l $HOME/genetico/mutations/mutacao_$MUTATION.txt | awk '{print $1}')

    echo "Count: $lcount"
    if((lcount==MAX_EXECUTION_COUNT))
    then
        ((MUTATION=MUTATION+INCREMENT_MUTATION))
    fi
done

import matplotlib.pyplot as plt
import networkx as nx
from networkx.algorithms import bipartite
import random
import os
from os.path import expanduser
import sys
from random import randint

def generateBipartiteGraph():
	g = nx.bipartite_preferential_attachment_graph([200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200], 1)

	return g


def addRandomness(g, p, count, total, positions):
	sys.stdout.write("Step " + str(count) + " of " + str(total) + '\n')
	if len(positions) == 0:
		return

	ne = nx.number_of_edges(g)#amount of edges
	te = len(nx.algorithms.bipartite.sets(g)[0]) * len(nx.algorithms.bipartite.sets(g)[1]) #k_max

	ae = int(float(te * p))
	if ne + ae > te:
		ae = int(te - ne)

	os.system('date')
	sys.stdout.flush()

	for i in range(0,ae):
		choice = randint(0,len(positions)-1)

		g.add_edge(positions[choice][0],positions[choice][1])

		del positions[choice]

		sys.stdout.write(str(i+1) + " of " + str(ae) + " | edges: " + str(nx.number_of_edges(g)) + '\r')
		sys.stdout.flush()

	print ""
	g = nx.Graph(g)

def archive(g):
	print "Saving..."

	home = expanduser("~")
	path = home + "/genetico/visits.txt"
	pathExhibition = home + "/genetico/exhibitions.txt"

	try:
		os.remove(path)
		os.remove(pathExhibition)
	except OSError:
		pass

	fileVisits = open(path, "w")
	fileExhibitions = open(pathExhibition, "w")

	for n in nx.algorithms.bipartite.sets(g)[0]:
		fileExhibitions.write("\n")
		fileExhibitions.write(str(n))

	fileExhibitions.close()

	for n in g.edges():
		fileVisits.write("\n")
		fileVisits.write(str(n[0]))
		fileVisits.write("@")
		fileVisits.write(str(n[1]))

	fileVisits.close()


def show_graph(g):
	pos = nx.spring_layout(g)
	s1 = nx.algorithms.bipartite.sets(g)[0]
	s2 = nx.algorithms.bipartite.sets(g)[1]
	nx.draw_networkx_nodes(g, pos, nodelist=s1, node_color='r')
	nx.draw_networkx_nodes(g, pos, nodelist=s2, node_color='b')
	nx.draw_networkx_edges(g, pos)
	plt.axis('off')
	plt.show()

def config(g):
	print('exhibitions = %d' % len(nx.algorithms.bipartite.sets(g)[0]))
	print('visitors = %d' % len(nx.algorithms.bipartite.sets(g)[1]))

	print('nodes = %d' % len(g.nodes()))
	print('edges = %d' % len(g.edges()))

	home = expanduser("~")
	path = home + "/genetico/config.txt"

	try:
		os.remove(path)
	except OSError:
		pass

	fileConfig = open(path, "w")

	fileConfig.write(str(len(nx.algorithms.bipartite.sets(g)[0])))
	fileConfig.write("\n")
	fileConfig.write(str(len(nx.algorithms.bipartite.sets(g)[1])))

	fileConfig.close()

def edgeExits(g, i, j):
	for edge in g.edges():
		if edge[0] == i:
			if edge[1] == j:
				return 1

	return 0


if __name__ == '__main__':
	g = generateBipartiteGraph()

	os.system('clear')

	positions = []

	for i in range(0,len(nx.algorithms.bipartite.sets(g)[0])):
		sys.stdout.write("Preparing... " + str(i+1) + " of " + str(len(nx.algorithms.bipartite.sets(g)[0])) + '\r')
		sys.stdout.flush()
		for j in range(0+len(nx.algorithms.bipartite.sets(g)[0]),len(nx.algorithms.bipartite.sets(g)[1])+len(nx.algorithms.bipartite.sets(g)[0])):
			if edgeExits(g, i, j) == 0:
				position = (i,j)
				positions.append(position)
	os.system('clear')

	total = 10

	for i in range(1,total):
		addRandomness(g,0.025,i,total, positions)

	archive(g)
	config(g)

	show_graph(g)

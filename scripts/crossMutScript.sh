#!/bin/bash

rm -rf $HOME/genetico/mutations/crossMut_*

CROSSOVER=100
MUTATION=0
ITERATOR=10
MAX_GENERATION=5000

MAX_MUTATION=50
MIN_CROSSOVER=0

CROSScpy=$CROSSOVER
ITERcpy=$ITERATOR

while((MUTATION <= MAX_MUTATION))
do
  while((CROSSOVER >= MIN_CROSSOVER))
  do
      while((ITERATOR > 0))
      do
          $HOME/genetico/genetico $MUTATION 0.1 $CROSSOVER 1 $MAX_GENERATION 0 0 1 0 0 10 10 1 0 1 1 0 3 3 0 0 150

          echo "-------------------------------------------------------"

          ((ITERATOR--))
      done
      ((ITERATOR=ITERcpy))
      ((CROSSOVER=CROSSOVER-10))
  done
  ((MUTATION=MUTATION+10))
  ((CROSSOVER=CROSScpy))
done

#!/bin/bash

rm -rf $HOME/genetico/logs/recommendation*

BASELINE=0
CUT=1

BASELINE_MAX=4
CUT_MAX=5
CUTcpy=$CUT

while((BASELINE <= BASELINE_MAX))
do
	while((CUT <= CUT_MAX))
	do
		$HOME/genetico/genetico 5 0.1 0 1 5000 0 0 1 0 1 10 10 1 0 0 0 1 $CUT $BASELINE 0 0 150
		((CUT+=1))
	done

	mv $HOME/genetico/logs/recommendation.txt $HOME/genetico/logs/recommendation$BASELINE.txt

	((BASELINE+=1))
	((CUT=CUTcpy))
done

#!/bin/bash

rm -rf $HOME/genetico/logs/log_crossover.txt

MAX_GENERATION=10000
CROSSOVER=100
MUTATION=5

ITERATOR=10
ITERATORcpy=$ITERATOR

while((CROSSOVER>=0))
do
    while((ITERATOR>0))
    do
        $HOME/genetico/genetico $MUTATION 0.2 $CROSSOVER 1 $MAX_GENERATION 1 0 0 1 1 10 10 1 0 0 1 0 3 3 0 0 150

        ((ITERATOR--))
    done
    ((ITERATOR=ITERATORcpy))
    ((CROSSOVER=CROSSOVER-10))
done

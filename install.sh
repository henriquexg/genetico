#!/bin/bash

sudo apt-get install cmake build-essen libglu1-mesa-dev freeglut3-dev mesa-common-dev
sudo apt-get install cmake build-essential octave python-pip
sudo python -m pip install -U pip setuptools
sudo python -m pip install matplotlib
